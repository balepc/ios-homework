module HomeWork

  class API < Grape::API

    version 'v1', using: :header, vendor: 'HomeWork'
    format :json

    get 'books.json' do
      [
          {:id=> 1, :title=>'MongoDB: The Definitive Guide', :author=>"Kristina Chodorow", :cover_image=>"http://akamaicovers.oreilly.com/images/0636920001096/cat.gif"},
          {:id=> 2, :title=>'Hadoop: The Definitive Guide', :author=>"Tom White", :cover_image=>"http://akamaicovers.oreilly.com/images/0636920010388/cat.gif"},
          {:id=> 3, :title=>'Patterns of Enterprise Application Architecture', :author=>"Martin Fowler", :cover_image=>"http://bookshop.pearson.de/media_local/shop_u1bigs/9780321127426.jpg"},
          {:id=> 4, :title=>'Seven Databases in Seven Weeks', :author=>"Eric Redmond", :cover_image=>"http://ecx.images-amazon.com/images/I/41rYtcbzOUL._SX258_BO1,204,203,200_.jpg"},
          {:id=> 5, :title=>'BioCoder #6', :author=>"O'Reilly Media", :cover_image=>"http://akamaicovers.oreilly.com/images/0636920036470/cat.gif"},
          {:id=> 6, :title=>'Doing Data Science', :author=>"Cathy O'Neil, Rachel Schutt", :cover_image=>"http://akamaicovers.oreilly.com/images/0636920028529/cat.gif"}
      ]
    end

    params do
      requires :id, type: Integer
    end
    post "/book/:id" do
      if params[:id].nil?
        {:status => "fail"}
      else
        { :status => "ok", :id=>params[:id], :name=>params[:name], :comment=>params[:comment] }
      end
    end

  end

end